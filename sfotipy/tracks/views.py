import json
from django.core import serializers
from django.http import HttpResponse, Http404
from django.shortcuts import render, get_object_or_404
# Create your views here.

from .models import Track

def track_view(request, title):

	#try:
	#	track = Track.objects.get(title=title)
	#except Track.DoesNotExist:
	#	raise Http404

	track = get_object_or_404(Track, title=title)
	bio = track.artist.biography
	
	data = {
		'title': track.title,
		'order': track.order,
		'album': track.album.title,
		'artist': {
			'name': track.artist.first_name,
			'bio': bio,
		}
	}

	json_data = json.dumps(data)
	#json.loads(string_json) Para volver al diccionario de python

	return HttpResponse(json_data, content_type='application/json')

	#return render(request, 'track.html', {'track':track})

